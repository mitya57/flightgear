Source: flightgear
Maintainer: Debian FlightGear Crew <team+flightgear@tracker.debian.org>
Uploaders: Ove Kaaven <ovek@arcticnet.no>,
           Markus Wanner <markus@bluegap.ch>
Section: games
Priority: optional
Build-Depends: cmake,
               debhelper-compat (= 13),
               flightgear-data-ai (>= 1:2020.3.18~),
               flightgear-data-base (>= 1:2020.3.18~),
               flite1-dev,
               libboost-dev,
               libcurl4-gnutls-dev,
               libdbus-1-dev,
               libevent-dev,
               libexpat1-dev,
               libgl-dev,
               libglu1-mesa-dev,
               libgsm1-dev,
               libhtsengine-dev,
               liblzma-dev,
               libopenal-dev,
               libopenscenegraph-dev,
               libplib-dev,
               libqt5svg5-dev,
               libsimgear-dev (<= 1:2020.3.999),
               libsimgear-dev (>= 1:2020.3.18+dfsg-2.1~),
               libspeex-dev,
               libspeexdsp-dev,
               libsqlite3-dev,
               libudev-dev [linux-any],
               libudns-dev,
               libusbhid-dev [kfreebsd-any],
               libx11-dev,
               qtbase5-dev [!armel !armhf],
               qtbase5-private-dev [!armel !armhf],
               qtdeclarative5-dev [!armel !armhf],
               qtdeclarative5-private-dev [!armel !armhf],
               qttools5-dev,
               zlib1g-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian/flightgear
Vcs-Git: https://salsa.debian.org/debian/flightgear.git
Homepage: https://home.flightgear.org/
Rules-Requires-Root: no

Package: flightgear
Architecture: any
Depends: flightgear-data-all (>= 1:2020.3.18~),
         qml-module-qtquick-window2 [!armel !armhf],
         qml-module-qtquick2 [!armel !armhf],
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: flightgear-phi
Conflicts: metar
Description: Flight Gear Flight Simulator
 FlightGear Flight Simulator (often shortened to FlightGear or FGFS)
 is a sophisticated free, completely open-source flight simulator
 framework, created by volunteers.
 .
 This package contains the runtime binaries.
